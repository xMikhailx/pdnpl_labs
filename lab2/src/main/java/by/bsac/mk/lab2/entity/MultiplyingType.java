package by.bsac.mk.lab2.entity;

public enum MultiplyingType {
    LEAF, STALK, SEED
}
