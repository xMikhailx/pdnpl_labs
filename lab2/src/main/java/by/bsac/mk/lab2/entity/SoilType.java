package by.bsac.mk.lab2.entity;

public enum SoilType {
    PODZOLIC, UNPAVED, SOD_PODZOLIC
}
