package by.bsac.mk.practical4.main;

import by.bsac.mk.practical4.action.StringAction;

public class Main {
    public static void main(String[] args) {
        String text = "However, I try to find an exit.";
        StringAction.showWordsWithMaxAndMinLength(text);
    }
}
