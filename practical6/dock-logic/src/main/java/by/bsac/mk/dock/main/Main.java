package by.bsac.mk.dock.main;

import by.bsac.mk.dock.dock.Dock;
import by.bsac.mk.dock.entity.Ship;
import by.bsac.mk.dock.exception.IoDockException;
import by.bsac.mk.dock.reader.DockFileReader;
import by.bsac.mk.dock.util.DockParser;

import java.util.List;

public class Main {
    public static void main(String[] args) throws IoDockException {
        Dock.getInstance().initializeCapacityAndDockContainersCounts(30, 20, 3);
        List<Ship> ships = DockParser.parse(DockFileReader.getInstance().readFromFile());
        ships.forEach(Ship::start);
    }
}
