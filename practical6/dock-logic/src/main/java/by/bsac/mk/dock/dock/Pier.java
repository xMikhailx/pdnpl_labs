package by.bsac.mk.dock.dock;

import by.bsac.mk.dock.entity.Ship;
import by.bsac.mk.dock.exception.LogicDockException;

public class Pier {
    Pier() {
    }

    public void loadOrUnloadContainers(Ship ship) throws LogicDockException {
        Dock.getInstance().loadOrUnloadContainers(ship);
    }

    public void close() throws LogicDockException {
        Dock.getInstance().putBackPier(this);
    }
}
