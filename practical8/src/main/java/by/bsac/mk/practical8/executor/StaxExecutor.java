package by.bsac.mk.practical8.executor;

import by.bsac.mk.practical8.exception.XmlParserException;
import by.bsac.mk.practical8.parser.FlowersStaxBuilder;

public class StaxExecutor {
    public static void main(String[] args) throws XmlParserException {
        FlowersStaxBuilder staxBuilder = new FlowersStaxBuilder();
        staxBuilder.buildListFlowers("practical8/src/main/resources/flowers.xml");
        staxBuilder.getFlowers().forEach(System.out::println);
    }
}
