package by.bsac.mk.practical8.executor;

import by.bsac.mk.practical8.exception.XmlParserException;
import by.bsac.mk.practical8.parser.FlowersSaxBuilder;

public class SaxExecutor {
    public static void main(String[] args) throws XmlParserException {
        FlowersSaxBuilder saxBuilder = new FlowersSaxBuilder();
        saxBuilder.buildFlowers("practical8/src/main/resources/flowers.xml");
        saxBuilder.getFlowers().forEach(System.out::println);
    }
}
