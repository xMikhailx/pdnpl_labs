package by.bsac.mk.practical8.executor;

import by.bsac.mk.practical8.exception.XmlParserException;
import by.bsac.mk.practical8.parser.FlowersDomBuilder;

public class DomExecutor {
    public static void main(String[] args) throws XmlParserException {
        FlowersDomBuilder flowersDomBuilder = new FlowersDomBuilder();
        flowersDomBuilder.buildListFlowers("practical8/src/main/resources/flowers.xml");
        flowersDomBuilder.getFlowers().forEach(System.out::println);
    }
}
